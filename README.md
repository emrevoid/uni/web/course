# course

web technologies course 2019/2020 - unibo cesena campus

## quiz

**Le risposte pubblicate di seguito devono essere consultate a scopo di
verifica, quindi soltanto dopo aver scelto la risposta che si considera più
opportuna.**

**Non mi ritengo in alcun modo responsabile per un eventuale uso scorretto delle
risposte, quale, ad esempio, la consultazione previo studio, ragionamento e
sottomissione della risposta ai docenti**

### Architettura

#### Domanda 1

Q: Nell'architettura del Web:

A: Il client inizia sempre l'interazione con i server

### HTML1

#### Domanda 1

Q: Quale di questi elementi non indica un metadato:

A: `<head>`

#### Domanda 2

Q: Il carattere «à»:

A: Usa 8 bit per la codifica ISO Latin

### HTML2

#### Domanda 1

Q: Quale struttura descrive correttamente una sezione intitolata «Principale»
con due sottosezioni intitolate «Uno» e «Due»:

A:
```html
<section>

    <h1> Principale </h1>
    <section>

        <h2> Uno </h2>

    </section>
    <section>

        <h2> Due </h2>

    </section>
</section>
```

#### Domanda 2

Q: Per inserire in una sezione l'indirizzo a lato, è opportuno utilizzare:

A: delle andate a capo con `<br/>`

#### Domanda 3

Q: Quale alt indichereste per questa immagine (fonte Repubblica, occhiello a
destra):

A: `""`

### HTML3

#### Domanda 1

Q: A quale di questi elementi è semanticamente corretto che punti il link `<a
href="#primaparte">`?

A: `<section id="primaparte">`

#### Domanda 2

Q: Considerare il seguente codice. Come viene visualizzata la lista
corrispondente dal browser?

A:
```html
<ol>
  <li> Tecnologie Web
  <ul>
       <li> HTML </li>
       <li> URI </li>
  </ul>
  </li>
  <li> Reti
    <ul>
       <li> HTTP </li>
     </ul>
  </li>
</ol>
```

![](./img/html3-d2.png)

### HTML4

#### Domanda 1

Q: Per trasmettere in modo sicuro una password è opportuno usare:

A: HTTPS e POST

#### Domanda 2

Q: L'utente deve poter scegliere una quantità da 1 a 5. Quale soluzione non
risponde alla richiesta?

A: Le checkbox da 1 a 5.

### CSS1

#### Domanda 1

Q: Qual è la specificità del seguente selettore `aside#left p.first a
img[src=‘logo.png']`:

A: 124

#### Domanda 2

Q: Quale di queste forme abbreviate non è equivalente alle altre:

A:
```css
margin: 20px 10px 10px;
```

### CSS2

#### Domanda 1

Q: Quale insieme di regole di stile consente al browser di rendere un testo come
quello mostrato qui sotto?

A:
```css
border-radius: 0px 30px;
border: 3px blue solid;
box-shadow: grey 15px 15px 10px;
background-color: rgba(0, 0, 255, 0.2);
color: blue;
```

#### Domanda 2

Q: Considerando la foto a sinistra, con quali regole css posso ottenere
l'effetto riportato nella foto a destra?

A:
```css
-webkit-filter: sepia(100%);
filter: sepia(100%);
border-radius: 150px;
opacity: 0.5;
```

### Javascript 1

#### Domanda 1

Q: Quali sono gli elementi discendenti di tbody?

A: tutti gli elementi `<tr>` e tutti gli elementi `<td>`.

#### Domanda 2

Q: In Javascript cosa stampa a console il seguente codice?

A: 42 (come Number).

### jQuery

#### Domanda 1

Q: Cosa produce l'istruzione JQuery
`$("li.corrente").next().css("background-color", "red");` dato il seguente
listato:

A: l'elemento `<li>` con testo "elemento 4" avrà un colore di sfondo rosso.

### Accessibilità

#### Domanda 1

Q: Quale di questi contrasti è sufficiente per il livello AAA:

A: foreground `#7affca` e background `#660c70`.

#### Domanda 2

Q: Per essere accessibile, questa tabella richiede:

![](./img/acc-d2.png)

A: `caption`.

### Web Design

#### Domanda 1

Q: Dato il colore `#5C0DAC`, quali sono colori complementari triadico ('triad'):

A: `#BDF300`, `#FFC600`, `#390470`

### PHP

#### Domanda 1

Q: In un sito di e-commerce che registra gli utenti esclusivamente per gestire
le loro ordinazioni sono necessari:

A: Cookie tecnici

#### Domanda 2

Q: Nel sito della domanda 1 (sito di e-commerce che registra gli utenti
esclusivamente per gestire le loro ordinazioni) la cookie law richiede:

A: Segnalare i cookie nell'informativa

### PHP Sintassi

#### Domanda 1

Q: Cosa contiene la variabile superglobale `$_REQUEST[]` in PHP?

A: È un array associativo che contiene il contenuto delle variabili
supergloabili `$_GET`, `$_POST` e `$_COOKIE`

### Laboratorio 1

#### Domanda 1

Q: Quale tra questi genera codice valido:

A:
```html
<!DOCTYPE html>
<html lang=‘it'>
   <head>
    <title>1</title>
   </head>
   <body>
     <div id="titolo">
       <h1>Titolo 1</h1>
     </div>

    <div id="centro">
        <ul>
           <li>item</li>
        </ul>
    </div>
   </body>
</html>
```

### Domanda 2

Q: Quale tra questi è un selettore di prossimità:

A: `p>section`.

### Laboratorio 2

#### Domanda 1

Q: Con quale proprietà è possibile gestire spazi bianchi e andate a capo:

A:`white-space`.

#### Domanda 2

Q: Qual è la proprietà di CSS3 che permette di specificare come devono essere
dimensionati gli elementi, definendo anche se il bordo è compreso nella
larghezza dell'elemento:

A: `box-sizing: border-box;`

### Laboratorio 3

#### Domanda 1

Q: Per specificare che è obbligatorio inserire un valore per un certo input si
usa l'attributo:

A: `required`

### Laboratorio 4

#### Domanda 1

Q: se voglio che un div occupi 500px con un `padding: 20px 10px 20px;` quale
valore devo specificare come `width`?

A: `480`.

#### Domanda 2

Q: quale tra questi è un selettore di attributo?

A: `a[title~="main"]`

### Laboratorio 5

#### Domanda 1

Q: Qual è l'istruzione atta a dichiarare variabili in JavaScript?

A: `var`.

### Laboratorio 6

#### Domanda 1

Q: Qual è il codice jQuery corretto per impostare il rosso come colore di sfondo
di tutti i paragrafi?

A: `$("p").css("background-color","red");`

### Laboratorio 7

#### Domanda 1

Q: Gli script PHP sono inclusi all'interno di delimitatorio, quali?

A: `<?php ... ?>`

#### Domanda 2

Q: Qual è il modo corretto di includere il file "dbManager.php"?

A: `<?php require("dbManager.php"); ?>`

### Preparazione compito

#### HTML

Scrivere il codice HTML5 accessibile e semanticamente corretto per realizzare un
documento che contenga due sezioni.  La prima sezione si intitola “dati
personali” e contiene un paragrafo con nome, cognome, anno di corso, matricola
(su righe diverse).  La seconda sezione, intitolata Attività, è divisa in due
sottosezioni che si intitolano “tirocinio” e “tesi” e contengono un paragrafo
vuoto.

Soluzione disponibile [qui](./src/prep01-html/). (10/10 punti)

#### CSS

Dato il file `esercizio_css.html` (che si trova nelle risorse della sezione
Preparazione al compito), realizzare il file .css (esterno) così da ottenere il
layout e lo stile riportati in css.png (browser: Chrome, situata in risorse),
tenendo in considerazione quanto segue:
- Tutti i font devono avere la stessa font-family, che deve essere Arial. La
  dimensione deve essere del 100%.
- L'elemento di intestazione `<header>` ha come sfondo un gradiente che varia
  dai seguenti colori (in diagonale, dall'angolo in alto a sinistra all'angolo
  in basso a destra): rosa, giallo, rosa, viola. La sua larghezza è pari al
  100%.  Margin e padding devono essere simili a quelli riportati nello
  screenshot. Il bordo inferiore è di colore viola ed ha uno spessore simile a
  quello mostrato nello screenshot.
- Il testo in `<header>` è di colore viola ed è allineato al centro.
- L'elemento `<nav>`, l'elemento `<aside>` e l'elemento `<section>` sono
  affiancati, uno accanto all'altro.
- L'elemento `<section>` deve occupare il 60% della larghezza e deve avere
  padding e margin simili a quelli mostrati nello screenshot.
- I paragrafi figli di `<section>` sono allineati sinistra e il testo è di
  colore nero.
- L'elemento `<nav>` deve occupare il 10% della larghezza e deve avere padding e
  margini simili a quelli mostrati nello screenshot.
- Gli elementi della lista in `<nav>` mostrano un quadratino come simbolo, sono
  allineati a sinistra ed hanno interlinea pari a 2 volte il valore di default.
  Lo sfondo di `<nav>` consiste in un gradiente uguale a quello di `<header>`
- I link in `<nav>` in caso di hover vengono mostrati con sfondo viola, colore
  bianco, in grassetto e con un padding pari ad almeno l'1%.
- In caso di hover sull'elelmento `<nav>` allora l'ordine dei colori del
  gradiente viene invertito.
- L'elemento `<aside>` deve occupare il 15% della larghezza e i suoi paragrafi
  devono avere colore viola.
- L'elemento `<footer>` ha come sfondo un gradiente che varia dai seguenti
  colori (in diagonale, dall'angolo in basso a destra all'angolo in alto a
  sinistra): rosa, giallo, rosa, viola. La sua larghezza è pari al 100%. Margin
  e padding devono essere simili a quelli riportati nello screenshot.
- Il testo in `<footer>` è di colore viola, è allineato al centro ed ha una
  dimensione pari all'80%.

Soluzione disponibile [qui](./src/prep02-css/). (10/10 punti)


#### Javascript / jQuery

Dato il file html `esercizio_javascript.html` in allegato, creare il codice
JavaScript o jQuery in modo tale che:

- Al caricamento della pagina vengano nascoste tutte le immagini, eccetto la
  prima.
- Al click sul bottone con testo "precedente" venga nascosta l'immagine
  attualmente mostrata e l'immagine precedente venga mostrata con effetto di
  dissolvenza lento. Se l'immagine attualmente visualizzata è la prima, deve
  essere visualizzata l'ultima.
- Al click sul bottone con testo "successiva" venga nascosta l'immagine
  attualmente mostrata e l'immagine successiva venga mostrata con effetto di
  dissolvenza lento. Se l'immagine attualmente visualizzata è l'ultima, deve
  essere visualizzata la prima.

> N.B. il codice HTML fornito non deve essere modificato.

> N.B.2 La soluzione deve essere abbastanza generale da non dover cambiare
> codice nel caso si aggiungesse una nuova immagine.

Soluzione disponibile [qui](./src/prep03-js/). (10/10 punti)

#### Theory

Descrivere cosa si intende in ambito Web con usabilità e cosa con accessibilità,
sottolineando le differenze.

Soluzione disponibile [qui](./src/prep04-theory/). (9/10 punti)

Commento del prof: "poco chiare le differenze".

#### PHP

Scrivere il codice PHP valido (ovvero che esegua correttamente su server web
Apache) che legga i dati che gli sono stati inviati tramite GET nelle variabili
"A" e "B".

In questa pagina, occorrerà:

- Controllare che le variabili "A" e "B" non siano nulle e che siano valide,
  ovvero che siano numeri positivi e che sul db ci siano numeri appartenenti a
  quell'insieme.
- Leggere tutti i numeri appartenenti a ciascun insieme su database e inserirli
  in due vettori distinti.
- Creare un nuovo vettore contenente l'unione dei due insiemi.
- Inserire sul db il nuovo insieme, usando come id dell'insieme il successivo
  all'id massimo.
- Dovete supporre che il db esista (nome database: giugno; nome tabella:
  insiemi; username: root, pw: ) e che la tabella "insiemi" sia strutturata e
  riempita secondo le istruzioni che trovate nel file `README_DB.txt`
- Consegnato solamente il file vostraemailunibo.php.

Esempio:
Con i dati di esempio presenti nel file `README_DB.txt`, passando in GET A=1 e
B=2, i seguenti numeri dovranno essere inseriti all'interno del db (colonna
valore): 19, 2, 14, 98, 1. Tutte queste righe avranno il valore 3 nella colonna
insieme.

Soluzione disponibile [qui](./src/prep05-php/).
