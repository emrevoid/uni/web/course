<h1>Hello Cloudreach!</h1>
<h4>Attempting MySQL connection from php...</h4>
<?php
$host = 'db'; // Must be the exact name of the docker-compose DB service
$user = 'root';
$pass = 'toor';
$db   = 'giugno';
$conn = new mysqli($host, $user, $pass, $db);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected to MySQL successfully! <br/>";

if (!isset($_GET["A"]) || !isset($_GET["B"])) {
    die("A or B are null!");
}

$A = $_GET["A"];
$B = $_GET["B"];

echo var_dump($A) . "<br/>";
echo var_dump($B) . "<br/>";

if(!is_numeric($A) || !is_numeric($B)) {
    die("A or B are not numbers!");
}

if($A < 0 || $B < 0) {
    die("A or B are NOT positive numbers!");
}

if($A == $B) {
    die("A and B cannot be equal!");
}

$pre_query = 'SELECT valore FROM `insiemi` where insieme = "';
$queryA = $pre_query . $A . '";';
$queryB = $pre_query . $B . '";';

$resultA = $conn->query($queryA);
$resultB = $conn->query($queryB);

if ($resultA->num_rows > 0 && $resultB->num_rows > 0) {
    $final = $resultA->fetch_all();
    $final = array_merge($final, $resultB->fetch_all());

    echo "FINAL<br/><pre>";
    print_r($final);
    echo "</pre>";

    //$final = array_unique($final);
    $final = array_map("unserialize", array_unique(array_map("serialize", $final)));
    echo "FINAL_UNIQUE<br/><pre>";
    print_r($final);
    echo "</pre>";

    $query = 'SELECT MAX(insieme) as max FROM `insiemi`';
    $res = $conn->query($query) or die(mysql_error());
    $res = $res->fetch_assoc();

    for($i = 0; $i < sizeof($final); $i++) {
        $final[$i]['insieme'] = $res['max'] + 1;
    }

    if(is_array($final)){
        foreach ($final as $row) {
            $fieldVal1 = mysql_real_escape_string($records[$row][0]);
            $fieldVal2 = mysql_real_escape_string($records[$row][1]);
            $fieldVal3 = mysql_real_escape_string($records[$row][2]);

            $query ="INSERT INTO insiemi (field1, field2, field3) VALUES ( '". $fieldVal1."','".$fieldVal2."','".$fieldVal3."' )";
            mysqli_query($conn, $query);
        }
    }
} else {
    echo "0 results";
}
$conn->close();
?>
