<h1>Hello Cloudreach!</h1>
<h4>Attempting MySQL connection from php...</h4>
<?php
// Dovete supporre che il db esista (nome: giugno, nome tabella "insiemi")
$host = 'db'; // Must be the exact name of the docker-compose DB service
$user = 'root';
$pass = 'toor';
$db   = 'giugno';
$conn = new mysqli($host, $user, $pass, $db);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Controllare che le variabili "A" e "B" non siano nulle
if (!isset($_GET["A"]) || !isset($_GET["B"])) {
    die("Must enter both A and B parameters!");
}

$A = $_GET["A"];
$B = $_GET["B"];

// Controllare che le variabili "A" e "B" siano numeri positivi
if((!is_numeric($A) || !is_numeric($B)) ||
    ($A < 0 || $B < 0) ||
    ($A === $B)) {
    die("A and B are not valid: they must be different positive numbers!");
}

// Using prepared statements
if (!($queryA = $conn->prepare("SELECT valore FROM insiemi where insieme = ?"))) {
    echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
}
if (!$queryA->bind_param("i", $A)) {
    echo "Binding parameters failed: (" . $queryA->errno . ") " . $queryA->error;
}
if (!$queryA->execute()) {
    echo "Execute failed: (" . $queryA->errno . ") " . $queryA->error;
}
$resultA = $queryA->get_result();

if (!($queryB = $conn->prepare("SELECT valore FROM insiemi where insieme = ?"))) {
    echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
}
if (!$queryB->bind_param("i", $B)) {
    echo "Binding parameters failed: (" . $queryB->errno . ") " . $queryB->error;
}
if (!$queryB->execute()) {
    echo "Execute failed: (" . $queryB->errno . ") " . $queryB->error;
}
$resultB = $queryB->get_result();

if ($resultA->num_rows > 0 && $resultB->num_rows > 0) {
    $arrA = $resultA->fetch_all(MYSQLI_ASSOC);

    echo "Valori contenuti nell'insieme ".$A.": ";
    foreach($arrA as $n) {
        echo $n["valore"].", ";
    }
    echo "<br/><br/>";

    $arrB = $resultB->fetch_all(MYSQLI_ASSOC);
    echo "Valori contenuti nell'insieme ".$B.": ";
    foreach($arrB as $n) {
        echo $n["valore"].", ";
    }
    echo "<br/><br/>";

    // Merging arrays
    //$final = $resultA->fetch_all();
    //$final = array_merge($final, $resultB->fetch_all());
    $final = array_merge($arrA, $arrB);

    // Mapping and removing duplicates
    $final = array_map("unserialize", array_unique(array_map("serialize",
        $final)));

    echo "Valori contenuti nell'insieme unito: ";
    foreach($final as $n) {
        echo $n["valore"].", ";
    }
    echo "\n";

    // Getting the max value for "insieme"
    $query = 'SELECT MAX(insieme) as max FROM `insiemi`';
    $res = $conn->query($query) or die(mysql_error());
    $max = $res->fetch_assoc()['max'];
    $max += 1;

    // INSERT
    foreach ($final as $row) {
        $query = 'INSERT INTO insiemi (valore, insieme) VALUES (' .
            $row["valore"] . ', ' . $max . ');';
        $conn->query($query);
    }
}

$conn->close();
?>
