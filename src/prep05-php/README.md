# PHP quiz

## Run

1. Start docker
2. `docker-compose up -d`
3. Go to `http://localhost:8080`, then login (username "root", password "toor")
4. On the left, press "SQL command", then paste the content of
   `./db/giugno.sql`, then press "Execute"
5. Visit `http://localhost?A=3&B=4`. Should get the result.
