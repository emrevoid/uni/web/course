# Theory quiz

L'accessibilità è la proprietà di un sistema utile a fornire un servizio senza
discriminazioni sulle eventuali disabilità dei soggetti che ne usufruiscono.

In ambito Web tale definizione trova spazio nelle linee guida WCAG che
definiscono specifiche per l'accessibilità di pagine web. Le indicazioni più
rilevanti fanno riferimento alla fruizione di contenuti audiovisivi, del testo
in più forme e all'utilizzo dei colori.

L'usabilità è la proprietà dell'interazione tra l'utente ed un sistema che ne
definisce la facilità con cui i servizi offerti possono essere usufruiti.

In ambito Web l'usabilità viene definita attraverso i punti cardine che
disciplinano il design dell'esperienza utente: rappresentazioni grafiche delle
schermate (mockup), prototipi, l'identificazione degli utenti e dei casi di
utilizzo del sistema.

In termini di paragone, entrambe le proprietà guardano all'utente, tuttavia
l'usabilità fa riferimento alle interfacce che permettono l'interazione tra
utente e sistema mentre l'accessibilità pone un focus ai soggetti con
disabilità.
