# Usabilià

L'usabilità è la capacità di una pagina web di soddisfare i propri utenti dal punto di vista della semplicità di utilizzo.
L'interfaccia deve essere:
- comprensibile
- intuitiva
- facilmente utilizzabile

# Accessibilità

L'accessibilità è la capacità di una pagina web di essere utilizzata da diverse tipologie di utilizzatori.
Deve consentire l'accesso ai contenuti:
- ad utenti con disabilità
- indipendentemente dallo strumento utilizzato (computer, tablet, smartphone, SmartTV, ecc.)

# Differenze

L'usabilità deve garantire che l'applicativo o il servizio sia intuitivo e semplice da utilizzare indipendentemente dall'utente, l'accessibilità deve garantire che il servizio sia accessibile da qualsiasi utente e su qualsiasi dispositivo.
