$(function() {
    var slider = $(".slider-image").children("img");

    slider.each(function() {
        if(! $(this).is(":first-child")) {
            $(this).hide();
        }
    });

    $(".foll").click(function() {
        var currentImage = getCurrentImage();
        currentImage.fadeOut("slow", function() {
            if($(this).is(":last-child")) {
                next = slider.first();
            } else {
                next = $(this).next("img");
            }
            next.fadeIn("slow");
        });
    });

    $("#prev").click(function() {
        var currentImage = getCurrentImage();
        currentImage.fadeOut("slow", function() {
            if($(this).is(":first-child")) {
                prev = slider.last();
            } else {
                prev = $(this).prev("img");
            }
            prev.fadeIn("slow");
        });
    });

    function getCurrentImage() {
        return slider.filter(":visible");
    }
});
